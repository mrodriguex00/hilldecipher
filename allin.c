#include<stdio.h>

int imprime(int A[10][10],int n);
int lee(int A[10][10],int n);
int inv(int a, int m);
int copia(int C[10][10],int A[10][10],int n);
int gauss(int C[10][10],int A[10][10],int n,int m);
int inver(int C[10][10],int A[10][10],int n,int m);
int multi(int C[10][10],int A[10][10],int B[10][10],int n,int m);

int main()
	{
	int A[10][10];
	int B[10][10];
	int C[10][10];
	int n;
	int m;
	int o;
	scanf("%i %i %i",&o,&n,&m);
	
	switch(o)
		{
		case 0:
			lee(A,n);
			inver(C,A,n,m);
			imprime(C,n);
			break;
		case 1:
			lee(A,n);
			lee(B,n);
			multi(C,A,B,n,m);
			imprime(C,n);
			break;
		}

	return(0);
	}


int inver(int C[10][10],int A[10][10],int n,int m)
	{
	int A_[10][10];
	int i,j;
	
	copia(A_,A,n);
	
	for(i=0;i<n;i++)
		{
		for(j=0;j<n;j++)
			{
			A_[i][j+n] = 0;
			}
		A_[i][i+n] = 1;
		}

	gauss(C,A_,n,m);

	for(i=0;i<n;i++)
		{
		for(j=0;j<n;j++)
			{
			C[i][j] = C[i][j+n];
			}
		}
	
	return(0);
	}

int copia(int C[10][10],int A[10][10],int n)
	{
	int i,j;
	for(i=0;i<n;i++)
		{
		for(j=0;j<n;j++)
			{
			C[i][j]=A[i][j];
			}
		}
	return(0);
	}

int gauss(int C[10][10],int A[10][10],int n,int m)
	{
	int i,j,k;
	int c;
	int a,b;
	int x,y;

	for(j=0; j<n; j++)
		{
		for(i=j+1; i<n; i++)
			{
			a = A[j][j]%m;
			b = A[i][j]%m;
			for(k=0; k<(n<<1); k++)
				{
				A[i][k] = ((a*A[i][k]%m)-(b*A[j][k]%m)+m)%m;
				}
			}
		}
		
	for(j=0; j<n; j++)
		{
		for(i=j+1; i<n; i++)
			{
			a = A[n-1-j][n-1-j]%m;
			b = A[n-1-i][n-1-j]%m;
			for(k=0; k<(n<<1); k++)
				{
				x = b*A[n-1-j][k]%m;
				y = a*A[n-1-i][k]%m;
				A[n-1-i][k] = (((x-y)%m)+m)%m;				
				}
			}
		}

	for(i=0;i<n;i++)
		{
		a=A[i][i];
		for(j=0;j<(n<<1);j++)
			{
			C[i][j] = ((inv(a,m) * A[i][j]) + m) % m;
			}
		}
	return(0);
	}

int multi(int C[10][10],int A[10][10],int B[10][10],int n,int m)
	{
	int i,j,k,c;
	
	for(i=0;i<n;i++)
		{
		for(j=0;j<n;j++)
			{
			c=0;
			for(k=0;k<n;k++)
				{
				c += (A[i][k]%m) * (B[k][j]%m); 
				}
			C[i][j] = c%m;
			}
		}
	
	return(0);
	}

int imprime(int A[10][10],int n)
	{
	int i,j;
	for(i=0;i<n;i++)
		{
		for(j=0;j<n;j++)
			{
			printf("%5i ",A[i][j]);
			}
		puts("");
		}
	}

int lee(int A[10][10],int n)
	{
	int i,j;
	for(i=0; i<n; i++)
		{
		for(j=0; j<n; j++)
			{
			scanf("%i",&A[i][j]);
			}
		}
	return(n);
	}

int inv(int a, int m)
	{
	int b;
	int x;

	for(b = 0; b < m; b++)
		{
		x = (a * b) % m;
		if(x == 1)
			{
			return(b);
			}
		}
	return(m);
	}
